#include <iostream>
#include <cmath>

class MyClass
{
private: int a=0, b=0;
public:
	void ShowVariables()
	{
		std::cout << a << " " << b << "\n";
	}
};

class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		std::cout << x << " " << y << " " << z << "\n";
	}
	double VectorLength()
	{
		return sqrt(x * x + y * y + z * z);
	}
private:
	double x, y, z;
};

int main()
{
	MyClass temp;
	temp.ShowVariables();

	Vector vtemp(3, 4, 5);
	vtemp.Show();
	std::cout << vtemp.VectorLength() << "\n";
}